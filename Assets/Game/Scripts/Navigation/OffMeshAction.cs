using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(OffMeshLink))]
public class OffMeshAction : MonoBehaviour
{
    public enum Action
    {
        StandToCover,
        CoverToStand
    };
    public Action action;
    public bool applyRotationBlend = true;
}
