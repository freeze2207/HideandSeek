using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationListener : MonoBehaviour, IAnimationCompleted
{
	#region Animation Move
	private UnityEvent onAnimationMoveEvent = new UnityEvent();

	public void AddAnimationMoveListener(UnityAction callback)
	{
		onAnimationMoveEvent.AddListener(callback);
	}

	public void RemoveAnimationMoveListener(UnityAction callback)
	{
		onAnimationMoveEvent.RemoveListener(callback);
	}

	private void OnAnimatorMove()
	{
		onAnimationMoveEvent.Invoke();
	}
	#endregion

	#region Animation Completed Callback
	[System.Serializable]
	public class IntUnityEvent : UnityEvent<int> { }
	private Dictionary<int, IntUnityEvent> animationCompletedEvents = new Dictionary<int, IntUnityEvent>();

	public void AddAnimationCompletedListener(int animationName, UnityAction<int> callback)
	{
		IntUnityEvent eventCallback;
		if (animationCompletedEvents.TryGetValue(animationName, out eventCallback))
		{
			eventCallback.AddListener(callback);
		}
		else
		{
			eventCallback = new IntUnityEvent();
			eventCallback.AddListener(callback);
			animationCompletedEvents.Add(animationName, eventCallback);
		}
	}

	public void RemoveAnimationCompletedListener(int animationName, UnityAction<int> callback)
	{
		IntUnityEvent eventCallback;
		if (animationCompletedEvents.TryGetValue(animationName, out eventCallback))
		{
			eventCallback.RemoveListener(callback);
		}
	}

	public void AnimationCompleted(int animationName)
	{
		IntUnityEvent eventCallback;
		if (animationCompletedEvents.TryGetValue(animationName, out eventCallback))
		{
			eventCallback.Invoke(animationName);
		}
	}
	#endregion
}
