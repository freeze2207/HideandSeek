using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class RunningState : HiderBaseState
{
    public float angularDampeningTime = 5.0f;
    public float deadZone = 10.0f;
    public float offMeshInbetweenRotation = 1.0f;

    private UnityAction onAnimationMoveCallback;
    private AnimationListener animationListener;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animationListener = owner.GetComponent<AnimationListener>();
        onAnimationMoveCallback = new UnityAction(onAnimationMove);
        animationListener.AddAnimationMoveListener(onAnimationMoveCallback);

        // Random select a place and set the path
        controller.chosenPoint = controller.hidingPlaces[Random.Range(0, controller.hidingPlaces.Count)];
        Vector3 point = controller.chosenPoint.transform.position;
        agent.SetDestination(point);
        NavMesh.CalculatePath(controller.transform.position, point, -1, controller.path);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (agent.desiredVelocity != Vector3.zero)
        {
            float speed = Vector3.Project(agent.desiredVelocity, transform.forward).magnitude * agent.speed;
            hiderAnimator.SetFloat("Speed", speed);

            float angle = Vector3.Angle(transform.forward, agent.desiredVelocity);
            if (Mathf.Abs(angle) <= deadZone)
            {
                transform.LookAt(transform.position + agent.desiredVelocity);
            }
            else
            {
                transform.rotation = Quaternion.Lerp(transform.rotation,
                                                     Quaternion.LookRotation(agent.desiredVelocity),
                                                     Time.deltaTime * angularDampeningTime);
            }
        }
        else
        {
            hiderAnimator.SetFloat("Speed", 0.0f);
            fsm.ChangeState(fsm.HidingState);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animationListener.RemoveAnimationMoveListener(onAnimationMoveCallback);
    }

    private void onAnimationMove()
    {
        agent.velocity = hiderAnimator.deltaPosition / Time.deltaTime;
    }
}
