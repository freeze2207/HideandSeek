using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BeingFoundState : HiderBaseState
{
    public float upStandSpeed = 0.01f;

    private AnimationListener animationListener;
    private UnityAction onAnimationMoveCallback;
    private UnityAction<int> onAnimationCompletedCallback;
    private int ANIM_Crying = Animator.StringToHash("Crying");
    public override void Init(GameObject _owner, FSM _fsm)
    {
        base.Init(_owner, _fsm);
        animationListener = owner.GetComponent<AnimationListener>();

        onAnimationMoveCallback = new UnityAction(OnAnimationMove);
        onAnimationCompletedCallback = new UnityAction<int>(OnAnimationCompleted);
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        hiderAnimator.SetTrigger("ToStand");

        animationListener.AddAnimationMoveListener(onAnimationMoveCallback);
        animationListener.AddAnimationCompletedListener(ANIM_Crying, onAnimationCompletedCallback);
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animationListener.RemoveAnimationMoveListener(onAnimationMoveCallback);
        animationListener.RemoveAnimationCompletedListener(ANIM_Crying, onAnimationCompletedCallback);
    }

    private void OnAnimationCompleted(int animationName)
    {
        agent.enabled = true;
        hiderAnimator.SetBool("IsMirror", false);
        fsm.ChangeState(fsm.RunningState);
    }

    private void OnAnimationMove()
    {
        transform.rotation = hiderAnimator.rootRotation;
        transform.position = hiderAnimator.rootPosition;
        
    }
}
