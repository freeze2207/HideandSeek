using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class HiderController : MonoBehaviour
{
    [HideInInspector]
    public NavMeshPath path;

    public List<GameObject> hidingPlaces = new List<GameObject>(10);
    public GameObject chosenPoint;

    // Start is called before the first frame update
    void Start()
    {
        path = new NavMeshPath();
    }

}
