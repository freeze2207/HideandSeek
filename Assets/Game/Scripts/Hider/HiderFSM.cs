using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HiderController))]
public class HiderFSM : FSM
{
    public readonly int RunningState = Animator.StringToHash("Running");
    public readonly int HidingState = Animator.StringToHash("Hiding");
    public readonly int BeingFoundState = Animator.StringToHash("BeingFound");
}
