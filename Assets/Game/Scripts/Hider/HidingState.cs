using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class HidingState : HiderBaseState
{
    public float hidingSpeed = 3.0f;

    private AnimationListener animationListener;
    private UnityAction onAnimationMoveCallback;

    private int ANIM_Hiding = Animator.StringToHash("StandToCover");

    public override void Init(GameObject _owner, FSM _fsm)
    {
        base.Init(_owner, _fsm);
        animationListener = owner.GetComponent<AnimationListener>();

        onAnimationMoveCallback = new UnityAction(OnAnimationMove);
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (Vector3.Angle(transform.forward, controller.chosenPoint.transform.GetChild(1).transform.right) < 90.0f)
        {
            hiderAnimator.SetBool("IsMirror", true);
        }

        hiderAnimator.SetTrigger("Hide");
        animationListener.AddAnimationMoveListener(onAnimationMoveCallback);

        GameManager.Instance.OnFound.AddListener(OnFoundTriggered);
        agent.enabled = false;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animationListener.RemoveAnimationMoveListener(onAnimationMoveCallback);

        GameManager.Instance.OnFound.RemoveListener(OnFoundTriggered);
    }

    private void OnAnimationMove()
    {
        Transform targetTransform = controller.chosenPoint.transform.GetChild(1);
        transform.position = Vector3.Lerp(transform.position, targetTransform.position, Time.deltaTime * hidingSpeed);
        //transform.position = hiderAnimator.rootPosition;
        transform.rotation = Quaternion.Lerp(transform.rotation,
                                                     targetTransform.rotation,
                                                     Time.deltaTime * hidingSpeed);
    }

    private void OnFoundTriggered()
    {
        fsm.ChangeState(fsm.BeingFoundState);
    }
}
